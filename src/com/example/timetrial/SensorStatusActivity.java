package com.example.timetrial;

import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.hardware.Sensor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SensorStatusActivity extends Activity {

	protected static MainActivity ctx;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ctx.sensors = this;
        
        doLayout();        
    }
    
    protected void doLayout() {
    	
        ViewGroup outer = (ViewGroup) getLayoutInflater().inflate(R.layout.sensorstatus, null);
        ViewGroup main = (ViewGroup) outer.getChildAt(0);

        // XXX update existing too
        // iterate children of viewgroup and findviewbyid 
        
		Map<Sensor, float[]> sData = ctx.loc.getSensorData();
		for (Map.Entry<Sensor, float[]> entry : sData.entrySet()) {
			StringBuilder sb = new StringBuilder();
			for (float f : entry.getValue()) {
				sb.append( Float.toString(f) + "|" );
			}
			
            View row = (View) getLayoutInflater().inflate(R.layout.sensorstatus_item, null);
            TextView txt = (TextView) row.findViewById(R.id.sensor_name);            
            txt.setText(entry.getKey().getName());
            txt = (TextView) row.findViewById(R.id.sensor_data);            
            txt.setText(sb.toString());
            main.addView(row);        	
			
		}
          
		List<logRoutes.runStats> runStats = ctx.loc.getRunStats();
		for (logRoutes.runStats rs : runStats) {
            View row = (View) getLayoutInflater().inflate(R.layout.sensorstatus_item, null);
            TextView txt = (TextView) row.findViewById(R.id.sensor_name);            
            txt.setText("RUN");
            // XXX get individual run stats
            txt = (TextView) row.findViewById(R.id.sensor_data);            
            txt.setText(rs.toString());
            main.addView(row);        									
		}
		
		// XXX add last updated
		
        setContentView(outer);
        
    }
    // invalidate and onDraw
    
}
