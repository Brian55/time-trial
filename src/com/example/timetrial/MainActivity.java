package com.example.timetrial;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import zephyr.android.BioHarnessBT.BTClient;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Menu;

import android.bluetooth.*;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;

import com.google.android.gms.common.*;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.*;

import android.util.Log;
import com.google.android.gms.location.*;
import android.location.*;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.hardware.Sensor;

public class MainActivity extends Activity implements android.location.LocationListener {

	private GoogleMap mMap;
	private LocationManager locationManager;
	
	private static Context context;
	protected SensorStatusActivity sensors; // Link to active sensor view
		
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

	private ImageButton startButton, stopButton;	
	private Button pauseButton;
	private TextView timerValue;
	private long startTime = 0L;
	private Handler customHandler = new Handler();
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;

	// Authentication Members
	private static String authentication_token;
	private static String authenticated_user;

	// Location Svc and Run/Route Members
	private LatLng lastPos;
		
	// Pace, Speed, Distance
	// BioHarness data: Heartrate, ...
	class RunStats {
		long start_time;
		long end_time;
		
		float distance;
		float speed, avg_speed; // Miles per hour
		
		// Pace: Min/Mile  
		// 60minutes/hour / 8 miles/hour = 7:30 minutes/mile
		// 1mph = 60 mins/mile
		int pace_min, pace_sec;
		int avg_pace_min, avg_pace_sec;
		
		// XXX bioharness
		
		public Map<String, String> toMap() {
			Map<String, String> data = new HashMap<String, String>();
			
			data.put("start_time", String.valueOf(start_time));
			data.put("end_time", String.valueOf(end_time));
			data.put("distance", String.valueOf(distance));
			
			data.put("speed", String.valueOf(speed));
			data.put("avg_speed", String.valueOf(avg_speed));
			
			data.put("pace", String.format("%02d:%02d", pace_min, pace_sec));
			data.put("avg_pace", String.format("%02d:%02d", avg_pace_min, avg_pace_sec));
			
			// XXX bioharness
			
			return data;
		}
				
	}
	protected RunStats runStats;

	class Endpoint {
		LatLng start;
		LatLng end;
	}
	class RouteSegments {		
		List<Endpoint> endpoints;
		LatLng start;
		LatLng end;
		float distance;
		int route_id;
		
		int challenge_id;
	}
	private List<Map<String, RouteSegments>> routes;
	protected static RouteSegments route; // Selected Route

	// Custom GPS (and external Sensor) tracking class
	logRoutes loc;

	
// XXX onResume for screen orientation changes so we can support landscape?

	
/** Called when the activity is first created. */
@Override
public void onCreate(Bundle savedInstanceState) {

	super.onCreate(savedInstanceState);

	setContentView(R.layout.main);
	
	lastPos = null;
	route = null;
	routes = new ArrayList<Map<String, RouteSegments>>();
	runStats = new RunStats();
	
	timerValue = (TextView) findViewById(R.id.mainText_timer);
	
	// Initialize google map
	mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
	mMap.getUiSettings().setMyLocationButtonEnabled(false); // Doesn't seem to work?
	mMap.setMyLocationEnabled(true);
	
	mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
		public boolean onMarkerClick(Marker m) {
			
			if (m.getTitle() == null) return false;
			Log.i( "marker",  m.getTitle() );
			
			// Find entry in routes matching name & start
			for (int i=0; i < routes.size(); i++) {				
				if (routes.get(i).containsKey( m.getTitle() )) {
					Log.i("marker", "Found match by Name");
					
					RouteSegments rs = routes.get(i).get(m.getTitle());
					float distance[] = {0,0,0};
					Location.distanceBetween( rs.start.latitude, rs.start.longitude, m.getPosition().latitude, m.getPosition().longitude, distance);
					
					if (distance[0] <= 10) {
						Log.i("marker", "Found match by Position");
						route = rs;
					}
					
				}
			}
			
			// set selected route, request locationupdates until user moves to start
			if (route != null) {
				// XXX clear map?				
				
				makeShortToast("Go to Start Position");

	        	LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500 /* minTimeInterval ms */, 1 /* minDistance m */, MainActivity.this);

			}
			
			return false;
		}
	});
		
	// XXX use last known location ... get from our server if passive_provider sucks	
	//http://stackoverflow.com/questions/15098878/using-locationlistener-within-a-fragment
	// Find current location using PASSIVE provider to received last known location	
    locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 400, (float)10 /* minDistance m */, this); 		
			
	// http://developer.android.com/training/implementing-navigation/nav-drawer.html
    String[] mTitles = getResources().getStringArray(R.array.main_drawer_items);
    mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    mDrawerList = (ListView) findViewById(R.id.list_slidemenu);
	   	    
	// Set the adapter for the list view
	mDrawerList.setAdapter(new ArrayAdapter<String>(this,
	            R.layout.drawer_list_item, mTitles));
	
	// Set the list's click listener
	mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this));
    
    //SensorStatusActivity.ctx = this;

	
    // Challenge Start and Stop        
    startButton = (ImageButton) findViewById(R.id.btnchallenge);
    stopButton = (ImageButton) findViewById(R.id.btnchallengestop);
    
    startButton.setOnClickListener(new View.OnClickListener() {
    	
    	// Start GPS and Sensor tracking stuff and follow user to route.END
    	private void start_step2() {    		

    		runStats = new RunStats(); // XXX reset previous ...
    		
    		// Stop button
        	startButton.setVisibility(View.INVISIBLE);
        	stopButton.setVisibility(View.VISIBLE);						
    		
    		//start timer
    		TextView timer = (TextView) findViewById(R.id.mainText_timer);
    		timer.setText("0:00.00");
    		
    		// Left Text (Pace)
    		TextView txt = (TextView) findViewById(R.id.mainText_left);
    		txt.setGravity(Gravity.CENTER);
    		txt.setText(Html.fromHtml("<b>Pace (min/mile):</b>"));
    		
    		// Right Text (Speed)
    		txt = (TextView) findViewById(R.id.mainText_right);
    		txt.setGravity(Gravity.CENTER);
    		txt.setText(Html.fromHtml("<b>Speed (mph):</b>"));
    		    		
			startTime = SystemClock.uptimeMillis();
			runStats.start_time = startTime;
			
			customHandler.postDelayed(updateTimerThread, 0);			
						
			//start tracking route
			loc = new logRoutes(MainActivity.this);
        	
        	LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500 /* minTimeInterval ms */, 1 /* minDistance m */, loc);
    		        	    		
    	}
    	
    	public void onClick(View v) {    		
    	
    		if (route == null) {
    			makeShortToast("Click a Route to Begin!");
    		}
    		else {
    			// A route was selected, check if user is close enough and start
    			float distance[] = {0,0,0};
    			Location.distanceBetween(lastPos.latitude, lastPos.longitude, route.start.latitude, route.start.longitude, distance);

    			// distance[0] = 5; // XXX
    			
    			if (distance[0] <= 10) {
    				// XXX clear map of other routes?
    				    				
        			start_step2();
        			
    				// challenge_create with route_id 
            		final MainActivity ctx = MainActivity.this;
            		new Thread(new Runnable() {
            	    	
            			public void run() {

            				Map<String, String> data = new HashMap<String, String>();
            				data.put("route_id", String.valueOf(route.route_id) );
            				
            				ourHttpClient client = new ourHttpClient(MainActivity.this);    				
            			    client.addHeader("X-Auth-Token", MainActivity.authentication_token);
            			    client.post("/challenge/create", data );
            			                		
            			    try {
            			    	route.challenge_id = client.json.getInt("result");
            				} catch (JSONException e) {
            					// TODO Auto-generated catch block
            					e.printStackTrace();
            				}
            			    
            			}
            			
            		}).start();
        			
    			}
    			else makeShortToast("Sorry, you aren't quite in the right place!");
    			    			    			 
    		}
    		
    		//status.setText("Heart_rate");
    		
		}
    });    
                                
    stopButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View v) {
			
    		runStats.end_time = SystemClock.uptimeMillis();
    		
    		// Take SCREENSHOT XXX UiDevice.takeScreenshot 
    		
    		// XXX if settings allow external storage ...
    		// XXX and ONLY post to server if route.distance is <= runStats.distance
    		
    		final MainActivity ctx = MainActivity.this;
    		new Thread(new Runnable() {
    	    	
    			public void run() {
    				
    				Map<String, String> data = ctx.runStats.toMap();
    				
    				ourHttpClient client = new ourHttpClient(MainActivity.this);    				
    			    client.addHeader("X-Auth-Token", MainActivity.authentication_token);
    			    client.post("/challenge/update/" + String.valueOf(route.challenge_id), data );
    			    
    			    // XXX start leaderboard intent
    			    
    			    route = null;
    			}
    			
    		}).start();
    		
			//stop tracking route
    		LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    		locationManager.removeUpdates(loc);    		
    		
            // Stop button
        	startButton.setVisibility(View.VISIBLE);
        	stopButton.setVisibility(View.INVISIBLE);
        	
        	customHandler.removeCallbacks(updateTimerThread);
        	
        	TextView txt = (TextView) findViewById(R.id.mainText_timer);
        	txt.setText("");        	
        	txt = (TextView) findViewById(R.id.mainText_left);
        	txt.setText("");        	
        	txt = (TextView) findViewById(R.id.mainText_right);
        	txt.setText("");        	
        	        	   	
		}
    });

}

public void updateLocation(LatLng pos) {

    lastPos = pos;
    
    final MainActivity ctx = this;    
	new Thread(new Runnable() {
    	
		public void run() {
			
			ourHttpClient client = new ourHttpClient(MainActivity.this);
			
		    client.addHeader("X-Auth-Token", MainActivity.authentication_token);
		    client.get("/update_location/" + String.valueOf(lastPos.latitude) + "/" + String.valueOf(lastPos.longitude) );
    	    		    
		    try {
				if (client.json != null) makeShortToast( client.json.getString("result") );
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
		    // ALSO get nearby routes and map start, finish, and line w/ click listener	    
		    client.get("/nearby_routes");

		    Map<String, RouteSegments> maprs = null;
		    
		    try {
				JSONObject it = client.json.getJSONObject("result");			
				JSONArray js = it.names();
								
				for (int i = 0; i < js.length(); i++) {										
					JSONObject j = it.getJSONObject( js.getString(i) );
					JSONArray a = j.getJSONArray( "points" );
					
					LatLng pos1 = null, pos2 = null;					
					
					// XXX random route colors?					
					int lineColor = Color.rgb( 0, 0, 200 );
										
					String name = j.getString("name");						
					float[] distance = {0,0,0};

					Endpoint pt = null;

					RouteSegments rs = new RouteSegments();
					rs.endpoints = new ArrayList<Endpoint>();
					rs.route_id = Integer.valueOf(js.getString(i));					
					
					for (int k=0; k < a.length(); k++) {
						JSONObject line = a.getJSONObject( k );
						
						JSONArray start = line.getJSONArray("start");
						JSONArray end = line.getJSONArray("end");						
						pos1 = new LatLng(start.getDouble(0), start.getDouble(1));						
						pos2 = new LatLng(end.getDouble(0), end.getDouble(1));					
					
						pt = new Endpoint();
						pt.start = pos1;
						pt.end = pos2;
						rs.endpoints.add(pt);
						
						if (rs.start == null) {
							rs.start = pos1; // INITIAL

							ctx.mapAddMarker(new MarkerOptions()
							.icon( BitmapDescriptorFactory.fromResource(R.drawable.green_flag) )
					        .title( name )
					        .position(pos1)); 												
						}
									
						ctx.mapAddPolyline(new PolylineOptions()
							.add(pos1, pos2)
							.width(10)
							.color(lineColor));							
						
						Location.distanceBetween(pos1.latitude, pos1.longitude, pos2.latitude, pos2.longitude, distance);
						rs.distance += distance[0];
						
					}

					if (pos2 != null) rs.end = pos2;
					// Convert Meters to Miles
					rs.distance = (float) (rs.distance * 0.000621371);
								
					maprs = new HashMap<String, RouteSegments>();
					maprs.put(name, rs);
					routes.add(maprs);
				
					if (pos2 != null && pos2 != rs.start) {
						Location.distanceBetween(rs.start.latitude, rs.start.longitude, pos2.latitude, pos2.longitude, distance);

						Log.i("distance", String.valueOf(distance[0]) );
						
						// Only add end marker if it is some distance away from start so flags dont overlap
						if (distance[0] > 10) {
							ctx.mapAddMarker(new MarkerOptions()
							.icon( BitmapDescriptorFactory.fromResource(R.drawable.red_flag) )
							.title( name )
							.position(pos2) );
						}
						
					}
					
				}						
				
				//Log.i( "json", it.toString() );
			
			} catch (JSONException e) {
				Log.v("json", "error");
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		    		    		  
		    		    
		}
		
	}).start();
	
}

// ***********************************************
//  		MainActivity LocationListener
// ***********************************************

// http://android-developers.blogspot.co.uk/2011/06/deep-dive-into-location.html 
@Override
public void onLocationChanged(final Location location) {
    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

    if (route == null) {
    	CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
    	mMap.animateCamera(cameraUpdate);
    	
    	locationManager.removeUpdates(MainActivity.this); // Only need one update here
    	
    	updateLocation(latLng);
    }
    else {
    	
    	float distance[] = {0,0,0};
    	Location.distanceBetween(latLng.latitude, latLng.longitude, route.start.latitude, route.start.longitude, distance);
    	
    	// XXX getAccuracy() in case our provider has a huge radius and triggers this erroneously...
    	
    	// distance[0] = 0; // XXXXX
    	lastPos = latLng;
    	
    	if (distance[0] <= 10) {
    		locationManager.removeUpdates(this);
    		
    		makeShortToast("Click Challenge to Start!"); 
    		
    	}
    	
    }
            	 	
}


public void mapAddMarker(final MarkerOptions opt) {	
	
	customHandler.postDelayed(new Runnable() {
		public void run() {
			mMap.addMarker(opt);			
		}
	}, 0);
	
}
public void mapAddPolyline(final PolylineOptions opt) {
	
	customHandler.postDelayed(new Runnable() {
		public void run() {
			mMap.addPolyline(opt);
		}
	}, 0);	
	
}

@Override
public void onProviderDisabled(String provider) {
}

@Override
public void onProviderEnabled(String provider) {
}

@Override
public void onStatusChanged(String provider, int status, Bundle extras) {
}

//http://stackoverflow.com/questions/3134683/android-toast-in-a-thread
public void makeShortToast(final String text) {
	runOnUiThread(new Runnable() {
		public void run() {
			Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();		
		}
	});		
}

public static void setAuthToken(String token) {
	MainActivity.authentication_token = token;
}
public static void setAuthUser(String user) {
	MainActivity.authenticated_user = user;
}
public static String getAuthUser() {
	return MainActivity.authenticated_user;
}
public static String getAuthToken() {
	return MainActivity.authentication_token;
}

public void mapLine(LatLng pos) {
	if (lastPos != null) {	
		Polyline line = mMap.addPolyline(new PolylineOptions()
				.add(lastPos, pos)
				.width(10)
				.color(Color.GREEN));
	}
	lastPos = pos;
}

//Timer Start
private Runnable updateTimerThread = new Runnable() {
	
	public void run() {

		timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

		updatedTime = timeSwapBuff + timeInMilliseconds;

		int secs = (int) (updatedTime / 1000);
		int mins = secs / 60;
		secs = secs % 60;
		int milliseconds = (int) (updatedTime % 1000);

		ImageButton stop = (ImageButton) findViewById(R.id.btnchallengestop);
		if (stop.getVisibility() != View.INVISIBLE) { 
							
			timerValue.setText("" + String.format("%02d", mins) + ":"
				+ String.format("%02d", secs) + ":"
				+ String.format("%03d", milliseconds));
		
			customHandler.postDelayed(this, 0);
		}
		
		// XXX when we update sensors continuously and for multi-user,
		// scalability may improve if we separate this into read/write atomic
		// operations kind of like twitter

	
		int ui_update = Integer.valueOf(getApplicationContext().getResources().getString(R.string.UI_UPDATE));
		//if ( (secs % ui_update) != 0) return; // XXX		 

		
		// Left Text (Pace)
		TextView txt = (TextView) findViewById(R.id.mainText_left);
		txt.setGravity(Gravity.CENTER);
		txt.setText(Html.fromHtml(String.format("<b>Pace (min/mile):</b><br>Current: %02d:%02d<br>Average: %02d:%02d", runStats.pace_min, runStats.pace_sec, runStats.avg_pace_min, runStats.avg_pace_sec)));
	
		// Right Text (Speed)
		txt = (TextView) findViewById(R.id.mainText_right);
		txt.setGravity(Gravity.CENTER);
		txt.setText(Html.fromHtml(String.format("<b>Speed (mph):</b><br>Current: %02.2f<br>Average: %02.2f", runStats.speed, runStats.avg_speed) ));
		
		// Check if they finished !
		float d[] = {-1,0,0};
		if (loc != null && loc.lastPos != null && route != null && route.end != null) 
			Location.distanceBetween(loc.lastPos.latitude, loc.lastPos.longitude, route.end.latitude, route.end.longitude, d);

		// In case they used a shortcut...check if distance logged is close to what route should be
		if (d[0] >= 0 && d[0] <= 5 && Math.abs(runStats.distance-route.distance) <= 10) {
			makeShortToast("You finished!");
			stop.performClick();
		}
		
		
		// XXX if stats_instance != null so we can update that too	
		
		/*
		
		 XXX this is for continuously posting sensor data to external server XXX 
		
		ourHttpClient client = new ourHttpClient(MainActivity.this);					
	    client.addHeader("X-Auth-Token", MainActivity.authentication_token);
	    
	    Map<String, String> data = new HashMap<String, String>();	    					
	    StringBuilder sb2 = new StringBuilder();
	    
		Map<Sensor, float[]> sData = loc.getSensorData();
		for (Map.Entry<Sensor, float[]> entry : sData.entrySet()) {
			StringBuilder sb = new StringBuilder();
			sb.append(entry.getKey().getName());
			sb.append("=");
			for (float f : entry.getValue()) {
				sb.append( Float.toString(f) + "|" );
			}
			
			sb2.append(sb.toString() + "&");
		}
		data.put("sensor", sb2.toString() );
		
		sb2.setLength(0);
		List<logRoutes.runStats> runStats = loc.getRunStats();
		for (logRoutes.runStats rs : runStats) {
			sb2.append(rs.toString() + "&");					
		}
		data.put("run", sb2.toString());		
				
		// and POST to challenge_poll XXX challenge_id
		client.post("/challenge_poll?challenge_id=1", data);
		
		// with result (line segment w/ color, and detailed overview)
		// update UI
		*/
	}

};	 
//Timer End


private class DrawerItemClickListener implements ListView.OnItemClickListener {
	
	private MainActivity mActivity;
	
	public DrawerItemClickListener(MainActivity cls) {
		mActivity = cls;
	}
	
	@Override
	public void onItemClick(AdapterView parent, View view, int position, long id) {
		/*
		<item>Home</item>
		<item>Challenge</item>
		<item>Stats</item>
		<item>Leaderboard</item>		
		<item>Profile</item>
		<item>Settings</item>
		<item>Logout</item>    

		<item>Chat</item> XXX
		<item>Nutrition</item> XXX
		*/
		Intent i;
		switch (position) {
		case 0: // Home Screen
			mActivity.setVisible(true);
			/*
			// XXX when this (re)loads from a back btn or here the previous state is forgotten
			i = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(i);
			*/
			break;
		case 1: // Challenge
			if (startButton != null) startButton.performClick();
			break;
		// XXX stats
		case 2: 
			// Leaderboard
			i = new Intent(getApplicationContext(), LeaderBoardActivity.class);
			startActivity(i);			
			break;
		// XXX chat
		case 3: // Profile
			i = new Intent(getApplicationContext(), ProfileActivity.class);
			startActivity(i);			
			break;
		case 4: // Settings
			i = new Intent(getApplicationContext(), SettingsActivity.class);
			startActivity(i);			
			break;
		case 5: // Logout

			new Thread(new Runnable() {
				public void run() {
					ourHttpClient client = new ourHttpClient(MainActivity.this);					
				    client.addHeader("X-Auth-Token", MainActivity.authentication_token);
				    client.get("/logout");					
				}
			}).start();
			
			i = new Intent(getApplicationContext(), LoginActivity.class);
			startActivity(i);
			
			finish();
			break;
		}
		mDrawerLayout.closeDrawer(mDrawerList);
	}
}

}


