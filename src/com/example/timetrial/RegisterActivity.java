package com.example.timetrial;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
 
public class RegisterActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.register);
 
        TextView loginScreen = (TextView) findViewById(R.id.link_to_login); 
        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() { 
            public void onClick(View arg0) {                

            	finish();
            	
            }
        });
        
        Button regScreen = (Button) findViewById(R.id.btnRegister); 
        // Listening to Login Screen link
        regScreen.setOnClickListener(new View.OnClickListener() { 
            public void onClick(View arg0) {                
        
            	// XXX disable click
            	new Thread(new Runnable() {
            		            	
            		public void run() {
            			
            			ourHttpClient client = new ourHttpClient(RegisterActivity.this);
            			            		    
            			Map<String, String> data = new HashMap<String, String>();
                    	
    					TextView inputs = (TextView) findViewById(R.id.reg_fullname);
            			data.put("name", inputs.getText().toString() );
            			inputs = (TextView) findViewById(R.id.reg_email);
            			data.put("email", inputs.getText().toString() );
            			inputs = (TextView) findViewById(R.id.reg_password);
            			data.put("pass", inputs.getText().toString() );
            			
            		    client.post("/register", data);
                	    		    
            		    try {
            				makeShortToast( client.json.getString("result") );
            			} catch (JSONException e) {
            				e.printStackTrace();
            			}
            	           	
            		    // Get authentication token from object and save/store it            	           	            	
            		    if (!client.json.isNull("error")) {
            		    	try {
            		    		makeShortToast(client.json.getString("error"));
            		    	} catch (JSONException e) {
            		    		// TODO Auto-generated catch block
            		    		e.printStackTrace();
            		    	}	
            		    }
            		    else {
            		    	try {
            		    		MainActivity.setAuthToken(client.json.getString("token"));
            		    		MainActivity.setAuthUser(client.json.getString("user"));
            														
            		    		Intent i = new Intent(getApplicationContext(), MainActivity.class);
            		    		startActivity(i);
						
            		    		// Close register screen
            		    		finish();						
            		    	} catch (JSONException e) {
            		    		// TODO Auto-generated catch block
            		    		e.printStackTrace();
            		    	}
            		
            		    }
            	            	
            		}
            }).start();
            	                        
            }
        });
    }

 // http://stackoverflow.com/questions/3134683/android-toast-in-a-thread
    public void makeShortToast(final String text) {
    	runOnUiThread(new Runnable() {
    		public void run() {
    			Toast.makeText(RegisterActivity.this, text, Toast.LENGTH_SHORT).show();		
    		}
    	});		
    }
    
}