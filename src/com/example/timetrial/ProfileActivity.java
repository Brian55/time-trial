package com.example.timetrial;

import android.app.Activity;
import android.os.Bundle;
import android.os.Looper;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;



import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ProfileActivity extends Activity {

	//for taking picture
	private ImageView image;
    public Bitmap bmp;
    String encodedImageString;
    
    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, actionCode);
    }
    
    
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    setContentView(R.layout.profile);
	    
	    String[] items = new String[] {"Male", "Female"};
	    final Spinner spinner = (Spinner) findViewById(R.id.gender_spinner);
	    final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
	                android.R.layout.simple_spinner_item, items);
	    
	    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    spinner.setAdapter(adapter);
	    
	    takePictureButtonEvent();
        
	    // Load Age, Gender, Weight, Picture from /users/profile/ID
	    new Thread(new Runnable() {
	    	public void run() {	    		
	    		
	    		ourHttpClient client = new ourHttpClient(ProfileActivity.this);    				
	    	    client.addHeader("X-Auth-Token", MainActivity.getAuthToken() );
	    	    client.get("/user/profile/" + String.valueOf( MainActivity.getAuthUser() ));

	    	    try {
					if (client.json != null && client.json.getJSONObject("result") != null) {
						JSONObject ret = (JSONObject) client.json.getJSONObject("result");
						
						final String weight = ret.getString("weight"), age = ret.getString("age");

						// Gender
						final String gender = ret.getString("gender");
						
						// Picture
						final String picture = ret.getString("picture");						
						
						ProfileActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									EditText txt = (EditText) findViewById(R.id.weight);
									if (!weight.equals("null")) txt.setText(weight);
									
									txt = (EditText) findViewById(R.id.Age);
									txt.setText(age);
															
									// Gender
									if (gender != null)
										spinner.setSelection(adapter.getPosition(gender));
									
									// Picture
									ImageView img = (ImageView) findViewById(R.id.result);
									
									if (picture != null) {
										try {
											
											byte[] image = Base64.decode(picture, Base64.URL_SAFE|Base64.NO_WRAP);
														
											Bitmap imageDecoded = BitmapFactory.decodeByteArray(image, 0, image.length);
										
											if (imageDecoded == null) Log.i("profile", "decodeBA=null");
										
											img.setImageBitmap(imageDecoded);
										}
										catch (IllegalArgumentException e) {
											e.printStackTrace();
										}
									}
									
									adapter.notifyDataSetChanged();		
								}
						});
																			
						makeShortToast("Loaded");
						
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	    	}
	    }).start();
	    
	    Button apply = (Button) findViewById(R.id.btnApply);
	    apply.setOnClickListener(new View.OnClickListener(){
	            @Override
	            public void onClick(View view){
	    	            	
	            	//post to "/user/profile/ID"
	        	    new Thread(new Runnable() {
	        	    	public void run() {	    		

	        	    		// Gender, Age, Weight, Picture
	    	            	Map<String, String> data = new HashMap<String, String>();
	    	            	
	    	            	EditText txt = (EditText) findViewById(R.id.Age);
	    	            	data.put("age", txt.getText().toString());
	    	            	txt = (EditText) findViewById(R.id.weight);
	    	            	data.put("weight", txt.getText().toString());
	    	            	data.put("gender", spinner.getSelectedItem().toString());
	    	            	
	    	            	ImageView img = (ImageView) findViewById(R.id.result);
	    	            	if (img != null) {
	    	            		img.buildDrawingCache();
		    	            	Bitmap b = img.getDrawingCache();
		    	            	ByteArrayOutputStream bs = new ByteArrayOutputStream();
		    	            	b.compress(Bitmap.CompressFormat.PNG, 100, bs);
		    	    	        byte[] outputChunk = bs.toByteArray();
		    	    	        encodedImageString = Base64.encodeToString(outputChunk, Base64.URL_SAFE|Base64.NO_WRAP);
								
		    	    	        Log.i("profile", encodedImageString);
		    	    	        
		    	            	data.put("picture", encodedImageString);	    	            		
	    	            	}
	    	            	else data.put("picture", "");
	    	            	
	        	    		ourHttpClient client = new ourHttpClient(ProfileActivity.this);    				
	        	    	    client.addHeader("X-Auth-Token", MainActivity.getAuthToken() );
	        	    	    client.post("/user/profile/" + String.valueOf( MainActivity.getAuthUser() ), data);
	        	    	    
	        	    	}
	        	    }).start();
	        	    
	            }
	    });
	    
	    }
        	  
	  @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	        // TODO Auto-generated method stub
	        super.onActivityResult(requestCode, resultCode, data);

	        if (requestCode == 1 && resultCode == RESULT_OK && data != null) {

	            bmp = (Bitmap) data.getExtras().get("data");
	            bmp = GetBitMapFromByteArray(GetByteArrayFromBitmap(bmp));
	            image = (ImageView) findViewById(R.id.result);
	            image.setImageBitmap(bmp);

	        }
	    }

	    public Bitmap GetBitMapFromByteArray(byte[] byteArray) {
	        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
	    }


	    public byte[] GetByteArrayFromBitmap(Bitmap bmp) {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
	        byte[] outputChunk = baos.toByteArray();
	        encodedImageString = Base64.encodeToString(outputChunk, Base64.DEFAULT);
	        return Base64.decode(encodedImageString, Base64.DEFAULT);
	    }



	    private void takePictureButtonEvent(){
	        Button button = (Button) findViewById(R.id.btn_camera);
	        button.setOnClickListener(new View.OnClickListener(){
	            @Override
	            public void onClick(View view){
	                dispatchTakePictureIntent(1);
	            }
	        });
	    }

	    public void makeShortToast(final String text) {
	    	runOnUiThread(new Runnable() {
	    		public void run() {
	    			Toast.makeText(ProfileActivity.this, text, Toast.LENGTH_SHORT).show();		
	    		}
	    	});		
	    }

}
