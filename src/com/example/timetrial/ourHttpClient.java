package com.example.timetrial;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.NameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.os.Looper;
import android.widget.TextView;

public class ourHttpClient {
	
	protected int status;
	protected String response;
	protected JSONObject json;	
	protected URL url;
	
	private Context ctx;
	private Activity act;
	private KeyStore sslKeys;
	private TrustManagerFactory tmf;
	private SSLContext sslContext;
	private Map<String, String> headers;
	
	private Object obj;
	private Method toastMethod;
	
	ourHttpClient(Context context) {		
		ctx = context;
			
		Class[] paramString = new Class[1];
		paramString[0] = String.class;
		
		obj = (Object) ctx;
		toastMethod = null;
		try {
			toastMethod = obj.getClass().getDeclaredMethod("makeShortToast", paramString);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		sslKeys = null;
		tmf = null;
		sslContext = null;
		
		setupSSL();
		Looper.prepare();
		
		status = 0;
		response = null;
		json = null;
		
		headers = new HashMap<String, String>();
		
	}
	
	public void callToast(String text) {
		if (toastMethod == null) return;
		
		try {
			toastMethod.invoke(obj, text);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void addHeader(String name, String val) {
		headers.put(name, val);
	}

	// Load self-signed certificate from bundled keystore
	private void setupSSL() {
		
    	// From: http://randomizedsort.blogspot.com/2010/09/step-to-step-guide-to-programming.html to secure our external communication with self-signed certificate
    	sslKeys = null;
		try {
			sslKeys = KeyStore.getInstance("BKS");

			// XXX set the password etc in strings?
			sslKeys.load( ctx.getResources().openRawResource(R.raw.timetrial), "trial123".toCharArray() );

			tmf = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			tmf.init(sslKeys);

	    	sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, tmf.getTrustManagers(), new SecureRandom());
		} catch (KeyStoreException e1) {
			e1.printStackTrace();
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (CertificateException e1) {
			e1.printStackTrace();
		} catch (NotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (KeyManagementException e1) {
			e1.printStackTrace();
		}
		
	}
	
	private boolean setURL(String href) {
		boolean ret = true;
		
	   	url = null;
		try {						
			url = new URL("https", 
					ctx.getApplicationContext().getResources().getString(R.string.server_uri),
					Integer.parseInt(ctx.getApplicationContext().getResources().getString(R.string.server_port)), 
					href);
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
			ret = false;
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			ret = false;
		} catch (NotFoundException e1) {
			e1.printStackTrace();
			ret = false;
		}
		
		return ret;
	}
	
	public void get(String href) {		
    			
		if (!setURL(href)) return;
		
    	HttpsURLConnection conn = null;
    	
    	// 1) Open Connection
		try {
			conn = (HttpsURLConnection) url.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;					
		}
		
		// 2) Set connection settings and headers
		try {
			conn.setDoInput(true);
			//conn.setDoOutput(true);
			conn.setRequestMethod("GET");
        	conn.setRequestProperty(HTTP.TARGET_HOST, url.getHost());        	
        	//conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        	
        	for (Entry<String, String> kv : headers.entrySet()) {
        		conn.setRequestProperty(kv.getKey(), kv.getValue());
        	}
        	
		} catch (ProtocolException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
    	conn.setSSLSocketFactory(sslContext.getSocketFactory());
    	
    	// 3) Connect to server
		try {
			conn.connect();
			//os = conn.getOutputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;					
		}
		
		/*
    	BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			makeShortToast("An error occurred");
			return;										
		}
    					
    	try {
        	inputs = (TextView) findViewById(R.id.reg_fullname);
    		writer.write("name=" + inputs.getText().toString());
    		inputs = (TextView) findViewById(R.id.reg_email);
    		writer.write("&email=" + inputs.getText().toString());            	            	
        	inputs = (TextView) findViewById(R.id.reg_password);
			writer.write("&pass=" + inputs.getText().toString() );
    		writer.flush();
    		writer.close();
			os.close();								
		} catch (IOException e1) {
			e1.printStackTrace();
			makeShortToast("An error occurred");
			return;										
		}           	
    	*/
		
		// 4) Get HTTP status code
    	try {
			status = conn.getResponseCode();					
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("Server returned: " + String.valueOf(status) );
			return;
		}
   	
    	//http://stackoverflow.com/questions/17543460/cant-post-data-to-site-using-httpurlconnection-on-android
    	// 5) WHEN HTTP STATUS==200 OK, read response    	
    	if (status == 200) {
    		BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
				while ((line = br.readLine()) != null) {
				    sb.append(line+"\n");
				}
				br.close();
                response = sb.toString();						
			} catch (IOException e) {
				e.printStackTrace();
				callToast("An error occurred");
				return;											
			}                    
    	}
    	else {
    		callToast("An error occurred");
    		return;
    	}
   	
    	// Once response is read, try to create JSON object with it
    	try {
			if (response != null) json = (JSONObject) new JSONTokener(response).nextValue();
			// XXX else throw exception(); ?
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	/* 
    	// Get authentication token from object and save/store it    	           	            	
    	if (!json.isNull("error")) {
    		try {
				makeShortToast(obj.getString("error"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else {
    		try {
    			MainActivity.setAuthToken(obj.getString("token"));
    			MainActivity.setAuthUser(obj.getString("user"));
    														
            	Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
				
            	// Close register screen

            	finish();
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    		
    	}
		*/
		
	}
	
	// XXX x-www-form-urlencoded as opposed to raw or something else
	public void post(String href, Map<String,String> data) {
		
		if (!setURL(href)) return;
		
    	HttpsURLConnection conn = null;
    	
    	// 1) Open Connection
		try {
			conn = (HttpsURLConnection) url.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;					
		}
		
		// 2) Set connection settings and headers
		try {
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
        	conn.setRequestProperty(HTTP.TARGET_HOST, url.getHost());        	
        	conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        	
        	for (Entry<String, String> kv : headers.entrySet()) {
        		conn.setRequestProperty(kv.getKey(), kv.getValue());
        	}
        	
		} catch (ProtocolException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
    	conn.setSSLSocketFactory(sslContext.getSocketFactory());
    	
    	OutputStream os = null;
    	
    	// 3) Connect to server
		try {
			conn.connect();
			os = conn.getOutputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;					
		}
		
		// Write the POST DATA to the output stream
    	BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;										
		}
    					
    	try {
    		int count = 0;

        	for (Entry<String, String> kv : data.entrySet()) {
        		
        		if (count > 0) writer.write("&");
        		writer.write( kv.getKey() + "=" + kv.getValue() );
        		
        		count++;
        	}
			
    		writer.flush();
    		writer.close();
			os.close();								
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("An error occurred");
			return;										
		}           	
		
		// 4) Get HTTP status code
    	try {
			status = conn.getResponseCode();					
		} catch (IOException e1) {
			e1.printStackTrace();
			callToast("Server returned: " + String.valueOf(status) );
			return;
		}
   	
    	//http://stackoverflow.com/questions/17543460/cant-post-data-to-site-using-httpurlconnection-on-android
    	// 5) WHEN HTTP STATUS==200 OK, read response    	
    	if (status == 200) {
    		BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
				while ((line = br.readLine()) != null) {
				    sb.append(line+"\n");
				}
				br.close();
                response = sb.toString();						
			} catch (IOException e) {
				e.printStackTrace();
				callToast("An error occurred");
				return;											
			}                    
    	}
    	else {
    		callToast("An error occurred");
    		return;
    	}
   	
    	// Once response is read, try to create JSON object with it
    	try {
			if (response != null) json = (JSONObject) new JSONTokener(response).nextValue();
			// XXX else throw exception(); ?
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    			
	}
	
}
