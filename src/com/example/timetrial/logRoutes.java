package com.example.timetrial;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class logRoutes implements android.location.LocationListener, SensorEventListener {
	private Activity ctx;
	
	// track total distance traveled (m) for this instance
	protected int total_distance;	
		
	// List to hold various statistics from GPS sensor (speed, etc)
	private List<runStats> stats;	
	public LatLng lastPos;
	private Location lastLoc;

	// Data structure to hold data obtained from sensors maintained by android sensor API	
	private Map<Sensor, float[]> sensorData;
	private List<Sensor> sensors;
	private SensorManager mSensorManager;

	private boolean addSensor(int sType) {

		Sensor s = mSensorManager.getDefaultSensor(sType);		
		if (s != null) {
			mSensorManager.registerListener(this, s, mSensorManager.SENSOR_DELAY_NORMAL);
			sensors.add(s);
			return true;
		}
		
		return false;
	}
		
	//mSensorManager.unregisterListener(this);	
	
	// XXX get and clear local member for simple duplicate handling
	public Map<Sensor, float[]> getSensorData() { 
		return sensorData;		
	}
	
	public List<runStats> getRunStats() {
		return stats;
	}
	
	/* XXX 
	 * Force a MainActivity so we have always have the right update methods? 
	 * -OR- 
	 * remove entirely and rely on challenge_poll server timer to update UI 
	 */
	public logRoutes(Activity context) {
		ctx = context;
		
		lastPos = null;
				
		sensorData = new HashMap<Sensor, float[]> ();
		mSensorManager = (SensorManager) context.getApplicationContext().getSystemService(Context.SENSOR_SERVICE);		
		sensors = new ArrayList<Sensor>();
		stats = new ArrayList<runStats>();
		
		/* We'll add these later
		addSensor(Sensor.TYPE_AMBIENT_TEMPERATURE); // XXX change min to API14				
		addSensor(Sensor.TYPE_RELATIVE_HUMIDITY); // XXX change min to API14
		addSensor(Sensor.TYPE_LIGHT);
		addSensor(Sensor.TYPE_PRESSURE);
		addSensor(Sensor.TYPE_LINEAR_ACCELERATION);
		*/
		
	}

	// Sensors

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {}

	@Override
	public void onSensorChanged(SensorEvent ev) {

		String tmp = ev.sensor.toString();	
		for (float key: ev.values) {
			tmp = tmp + Float.toString(key);	
		}	
		Log.i("sensor", tmp);
		
		sensorData.put(ev.sensor, ev.values);
		
	}

	// Location Service/GPS Events
	
	@Override
    public void onLocationChanged(Location loc) {
		MainActivity act = (MainActivity) ctx;
		
		// XXX verify each route segment ...
		
		if (lastLoc != null) {
			float d = (float) (loc.distanceTo(lastLoc) * 0.000621371);
			
			act.runStats.distance += d;
		}
		act.runStats.speed = (float) (loc.getSpeed() * 2.23694); // MS to MPH
		
		if (act.runStats.avg_speed == 0) act.runStats.avg_speed = act.runStats.speed;
		else act.runStats.avg_speed = (act.runStats.speed + act.runStats.avg_speed)/2; // Naive average..

		if (act.runStats.speed > 0) {
			float tmpf = 60 / act.runStats.speed;		
		
			act.runStats.pace_min = (int) tmpf;
			act.runStats.pace_sec = (int) ((tmpf - act.runStats.pace_min) * 60);

			if (act.runStats.avg_pace_min == 0) {
				act.runStats.avg_pace_min = act.runStats.pace_min;
				act.runStats.avg_pace_sec = act.runStats.pace_sec;			
			}
			else {
				act.runStats.avg_pace_min = (act.runStats.pace_min+act.runStats.avg_pace_min)/2;
				act.runStats.avg_pace_sec = (act.runStats.pace_sec+act.runStats.avg_pace_sec)/2;			
			}
		}	
		
		runStats tmp = new runStats();
        
		if (lastLoc != null) {
			total_distance += loc.distanceTo(lastLoc);
		}
		lastLoc = loc;
		LatLng pos = new LatLng(loc.getLatitude(), loc.getLongitude());
		lastPos = pos;
		
        tmp.pos = pos;
        tmp.provider = loc.getProvider();
        tmp.accuracy = loc.getAccuracy();
        tmp.timestamp = loc.getTime();
        tmp.altitude = loc.getAltitude();
        tmp.bearing = loc.getBearing();
        tmp.speed = loc.getSpeed();
        //tmp.mock = loc.isFromMockProvider(); // XXX API18    
        
        stats.add(tmp);
        	
         // Add line to map
        act.mapLine(pos); // XXX remove this when multi-user updates from server is done
                
    }

    public void onProviderDisabled(String provider) {}

    public void onProviderEnabled(String provider) {}

    public void onStatusChanged(String provider, int status, Bundle extras) {}

    
class runStats {
	LatLng pos;
	String provider;
	float accuracy; // m
	
	long timestamp; // ms since 1/1/70 
	double altitude; // m above sealevel
	float bearing; // deg
	
	float speed; // m/s over ground
	
	boolean mock; 

	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("timestamp=" + Float.toString(timestamp) );
		sb.append("|latlng=" + pos.toString() );
		sb.append("|provider=" + provider );
		sb.append("|accuracy=" + Float.toString(accuracy) );
		
		sb.append("|altitude=" + Double.toString(altitude) );
		sb.append("|bearing=" + Float.toString(bearing) );
		
		sb.append("|speed=" + Float.toString(speed) );
		sb.append("|mock=" + mock );
		
		return sb.toString();
	}
}

}    

