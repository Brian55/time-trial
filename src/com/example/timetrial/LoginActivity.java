package com.example.timetrial;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
 
public class LoginActivity extends Activity {
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.login);       
                
        // Listening to register new account link
        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        registerScreen.setOnClickListener(new View.OnClickListener() { 
            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        
        // Listening to login button
        final Button button = (Button) findViewById(R.id.btnLogin);       
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	// XXX disable click
            	new Thread(new Runnable() {
            		            	
            		public void run() {

            			ourHttpClient client = new ourHttpClient(LoginActivity.this);
            		    
            			Map<String, String> data = new HashMap<String, String>();

    					TextView inputs = (TextView) findViewById(R.id.login_email);
            			data.put("email", inputs.getText().toString() );
            			inputs = (TextView) findViewById(R.id.login_password);
            			data.put("password", inputs.getText().toString() );
            			
            		    client.post("/login", data);                	    		   
            	
            		    // Get authentication token from object and save/store it            	           	            
            		    if (!client.json.isNull("error")) {
            		    	try {
            		    		makeShortToast(client.json.getString("error"));
            		    	} catch (JSONException e) {
            		    		//	TODO Auto-generated catch block
            		    		e.printStackTrace();
            		    	}
            		    }
            		    else {
            		    	try {
            		    		MainActivity.setAuthToken(client.json.getString("token"));
            		    		MainActivity.setAuthUser(client.json.getString("user"));
            													
            		    		Intent i = new Intent(getApplicationContext(), MainActivity.class);
            		    		startActivity(i);
		                
            		    		// finish() LoginActivity ? XXX
						
            		    	} catch (JSONException e) {
            		    		// TODO Auto-generated catch block
            		    		e.printStackTrace();
            		    	}
            		
            		    }
            	            	
            		}
            }).start();
            	
            }
        });
                       
    }
    
// http://stackoverflow.com/questions/3134683/android-toast-in-a-thread
public void makeShortToast(final String text) {
	runOnUiThread(new Runnable() {
		public void run() {
			Toast.makeText(LoginActivity.this, text, Toast.LENGTH_SHORT).show();		
		}
	});		
}

}