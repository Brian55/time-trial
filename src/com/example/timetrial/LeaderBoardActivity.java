package com.example.timetrial;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.widget.ExpandableListView;
import android.widget.Toast;


public class LeaderBoardActivity extends Activity {
	  // more efficient than HashMap for mapping integers to objects
	  SparseArray<Group> groups = new SparseArray<Group>();

	  MyExpandableListAdapter adapter;
	  
	  @Override
	  protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.leaderboard);
	    
	    if (MainActivity.route != null) {
			new Thread(new Runnable() {
				public void run() {
					ourHttpClient client = new ourHttpClient(LeaderBoardActivity.this);					
				    client.get("/leaderboard");
				   
				    int board_id = 0;
				    
				    if (client.json != null) {
				    	try {				    		
							JSONArray ja = client.json.getJSONArray("result");
							
							for (int i=0; i < ja.length(); i++) {
								JSONObject j = ja.getJSONObject( i );
								
								if (j.getInt("route_id") == MainActivity.route.route_id) board_id = j.getInt("board_Id");
								
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}				    	
				    }
				   				   
				    client.get("/leaderboard/get/" + String.valueOf(board_id) );
				    
				    if (client.json != null) {
				    	try {				    		
							JSONArray ja = client.json.getJSONArray("result");
							
							int k = groups.size();
							
							for (int i=0; i < ja.length(); i++) {
								JSONObject j = ja.getJSONObject( i );
								
								Group g = new Group( j.getString("full_name") );
								g.children.add( j.getString("elapsed_time") + " seconds" );
								
								groups.append(k, g);
								k++;
							}
							
							LeaderBoardActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									adapter.notifyDataSetChanged();		
								}
							});
							
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}				    	
				    }
				    
				}
			}).start();
	    	
		    ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
		    adapter = new MyExpandableListAdapter(this,
		        groups);
		    listView.setAdapter(adapter);
	
	    }
	    else {
	    	makeShortToast("Select A Route");
	    	finish();
	    }
	    	    
	  }

	  
	  public void makeShortToast(final String text) {
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(LeaderBoardActivity.this, text, Toast.LENGTH_SHORT).show();		
				}
			});		
		}
	  
	} 