import json

from flask import Flask, g
from flask import request, make_response, abort
from functools import update_wrapper

app = Flask(__name__)


from OpenSSL import SSL
context = SSL.Context(SSL.SSLv23_METHOD)
context.use_privatekey_file('apache.key')
context.use_certificate_file('apache.pem')

from config import *

import oursql 
from passlib.hash import phpass as crypt

from datetime import *
import itsdangerous
import hashlib

from werkzeug.routing import FloatConverter as BaseFloatConverter
class FloatConverter(BaseFloatConverter):
    regex = r'-?\d+(\.\d+)?'
app.url_map.converters['float'] = FloatConverter



@app.route("/")
def home():
	return "hello world"

@app.route("/login", methods=['POST'])
def login():
	resp = {}

	try:
		user = request.form['email']
		pw = request.form['password']

		resp = login_user(user, pw)
	except Exception as e:
		resp['error'] = str(e)

	return json.dumps(resp)

''' import traceback
traceback.print_stack()
'''

def login_user(email, pw):
	resp = {}
	try:
                db=get_db()
                curs=db.cursor(oursql.DictCursor)

                curs.execute("SELECT * FROM users WHERE email = ?", [email])
                user = curs.fetchone()

		if not user: raise Exception("Invalid user/password")

                if not crypt.verify(pw, user['password']): raise Exception("Invalid user/password")

		s = itsdangerous.TimestampSigner(key)
                resp['token'] = s.sign(str(user['user_id']))
                resp['user'] = user['user_id'] # XXX? user['full_name']

		data = (datetime.now(), user['user_id'])
		curs.execute("UPDATE users SET last_seen=? WHERE user_id = ?", data)

                data = (user['user_id'], datetime.now(), request.remote_addr, resp['token'])
                curs.execute("INSERT INTO user_sessions (user_id, date_created, ip_address, token) VALUES (?, ?, ?, ?)", data)

		resp['result'] = "Success"
        except Exception as e:
                resp['error'] = str(e)
	return resp

def login_auth(token):
		try:
			s = itsdangerous.TimestampSigner(key)
			uid = s.unsign(token, max_age=age)
		except:
			return False
		
                db=get_db()
                curs=db.cursor(oursql.DictCursor)
                data = (uid, token)
		ret = False

		try:
                	curs.execute("SELECT COUNT(*) as cnt FROM user_sessions WHERE user_id=? AND token=?", data)
			row=curs.fetchone()
			if row["cnt"] == 1: ret = uid
		except:
			pass

		return ret

@app.route("/register", methods=['POST'])
def register():
	resp = {}
	db = None 
	
	try:
		name = request.form['name']
		email = request.form['email']
		hash = crypt.encrypt(request.form['pass'])

		if len(name) <= 0: raise Exception("Name is required")
		if len(request.form['pass']) <= 0: raise Exception("Password is required")

		from validate_email import validate_email
		if len(email) <= 0 or not validate_email(email):
			raise Exception("Invalid email")

		db=get_db()
		curs=db.cursor(oursql.DictCursor)
		data = (name, email, hash, datetime.now())
		curs.execute("INSERT INTO users (full_name, email, password, date_created) VALUES (?, ?, ?, ?)", data)

		# XXX catch existing email error

		resp = login_user(email, request.form['pass'])
	except Exception as e:
		resp["error"] = str(e)

	print resp
	return json.dumps(resp)

def auth_required():
	def decorator(f):
		def wrapped(*args, **kwargs):
			token = request.headers['X-Auth-Token'] or False
			uid=login_auth(token)
			if not uid: abort(401)

			kwargs["uid"] = uid
			return f(*args, **kwargs)
		return update_wrapper(wrapped, f)
	return decorator


# logout
@app.route("/logout")
@auth_required()
def logout(**kwargs):
	resp = {}
	db = None
	try:
		token=request.headers['X-Auth-Token'] or False
                db=get_db()
                curs=db.cursor(oursql.DictCursor)
                curs.execute("UPDATE user_sessions SET token='' WHERE token=? LIMIT 1", [token])
		resp['result'] = "Success"	
	except Exception as e:
		resp['error'] = str(e)

	return json.dumps(resp)


@app.route("/update_location/<float:lat>/<float:long>")
@auth_required()
def update_location(lat, long, **kwargs):
	resp = {}

	try:
	        db=get_db()
        	curs=db.cursor(oursql.DictCursor)
		data = (datetime.now(), lat, long, kwargs['uid'])
	        curs.execute("UPDATE users SET last_seen=?,last_lat=?,last_long=? WHERE user_id=? LIMIT 1", data)
		resp['result'] = "Success"
	except:
		resp['result'] = "Error"

	return json.dumps(resp)


# backup db: mysqldump -u root -p android_timetrial -R --no-data >timetrial.sql

# usf 28.058703, -82.413854
# tampa 27.99629 -82.4542
# miami 25.78232 -80.23108

@app.route("/nearby_people")
@auth_required()
def nearby_people(**kwargs):
	# get self user object
	# using get_distance() find people within max radius in miles
	sql = "SELECT get_distance(u.last_lat, u.last_long) as distance from users u where u.user_id <> 1 GROUP BY u.user_id HAVING distance <= 25"

	# support gravatars

	resp = {}
	return json.dumps(resp)

# XXX nearby_places -- or only implement this in android via google places api?

@app.route("/nearby_routes")
@auth_required()
def nearby_route(**kwargs):
	resp = {}
	uid = kwargs['uid'] 
	sql = "SELECT * from users where user_id = ?"
	try:
	        db=get_db()
        	curs=db.cursor(oursql.DictCursor)
	        curs.execute(sql, [uid])	
		user=curs.fetchone()
		curs.close()

		sql = "SELECT get_distance(s.lat, s.lon, ?, ?) as distance, r.route_id, r.route_name as name from route_segments s, routes r where r.route_id = s.route_id group by s.route_id HAVING distance <= 50 LIMIT 25"
		# XXX LIMIT

		routes = {}
		data = (user['last_lat'], user['last_long'])
		curs = db.cursor(oursql.DictCursor)
		curs.execute(sql, data)
		row = curs.fetchone()
		while row is not None and row is not False:
			c2=db.cursor(oursql.DictCursor)
			sql = "SELECT lat, lon from route_segments where route_id = ? ORDER BY sequence_id ASC"
			c2.execute(sql, [row['route_id']])
			start = None
			l = []
			row2 = c2.fetchone()
			while row2 is not None and row2 is not False:
				if start is None:
					start = (row2['lat'], row2['lon'])
				else:
					l.append({"start":start, "end":(row2['lat'], row2['lon'])})
					start = (row2['lat'], row2['lon'])	
				row2 = c2.fetchone()
			routes[row['route_id']] = {"name":row['name'],"points":l}
			c2.close()
			row = curs.fetchone()
	
		# return all segments from start-finish so client can draw line
		resp['result'] = routes

	except Exception as e:
		import traceback
		traceback.print_stack()
		traceback.print_exc()
		resp['error'] = str(e)

	return json.dumps(resp)

'''
XXX todo if we have time
# chat
@app.route("/chat", methods=['POST'])
@auth_required()
def chat(**kwargs):
	# lat, long, message, uid via auth_required
	resp = {}
	return json.dumps(resp)

@app.route("/chat/<int:last_id>", methods=['GET'])
@auth_required()
def chat_view(last_id, **kwargs):
	# based on uid/last chat_id#
	# send new messages
	resp = {}
	return json.dumps(resp)
'''


# XXX we could notify *all* users both logged in and via google cloud/push messages based on last known location
# or only invite selected users based on map view of nearby people_search
# challenge_create
@app.route("/challenge/create", methods=['POST'])
@auth_required()
def challenge_create(**kwargs):
	resp = {}
	uid = kwargs['uid']
       	try:
                db=get_db()
                curs=db.cursor(oursql.DictCursor)

                data = (uid, request.form['route_id'], datetime.now())
                curs.execute("INSERT INTO challenge (user_id, route_id, date_created) VALUES (?, ?, ?)", data)

                resp['result'] = curs.lastrowid
        except:
                resp['error'] = "Error creating challenge"

	return json.dumps(resp)

# challenge_update
@app.route("/challenge/update/<int:challenge_id>", methods=['POST'])
@auth_required()
def challenge_update(challenge_id, **kwargs):
	resp = {}
        uid = kwargs['uid']
        try:
                db=get_db()
                curs=db.cursor(oursql.DictCursor)

		payload = dict(request.form)
		payload['user_id'] = uid
		payload = json.dumps(payload)
                data = (challenge_id, 'runStats', payload, datetime.now())
                curs.execute("INSERT INTO challenge_data (challenge_id, data_type, data_payload, date_created) VALUES (?, ?, ?, ?)", data)
		curs.close()

		curs=db.cursor(oursql.DictCursor)
		curs.execute("SELECT route_id FROM challenge where challenge_id=?", [challenge_id])
		row = curs.fetchone()
		route_id = row['route_id']		
		curs.close()

		# UPDATE LEADERBOARD
                curs=db.cursor(oursql.DictCursor)
		curs.execute("INSERT IGNORE INTO leaderboard (route_id) VALUES (?)", [route_id])
		curs.close()

		curs=db.cursor(oursql.DictCursor)
		curs.execute("SELECT board_id FROM leaderboard WHERE route_id=?", [route_id])
		row=curs.fetchone()
		board_id=row['board_id']
		curs.close()

		curs=db.cursor(oursql.DictCursor)
                data = (board_id, uid, datetime.now(), int(request.form['end_time'])-int(request.form['start_time']), json.dumps(request.form) )
                curs.execute("INSERT INTO leaderboard_users (board_id, user_id, date_created, elapsed_time, sensor_data) VALUES (?, ?, ?, ?, ?)", data)
		curs.close()

                resp['result'] = "Success"
        except:
		import traceback
		traceback.print_stack()
		traceback.print_exc()
                resp['error'] = "Error update challenge"


	# also create/update leaderboard for them on route

	return json.dumps(resp)

# challenge_poll XXX for multi-user positions
'''
@app.route("/challenge_poll?challenge_id=<int:id>", methods=['POST'])
@auth_required()
def challenge_poll(id, **kwargs):
	resp = {}
	print request.form

	return json.dumps(resp)
'''

# stats
@app.route("/leaderboard")
def leaderboard():
	resp = {}
	try:
		db=get_db()
                curs = db.cursor(oursql.DictCursor)
		sql="SELECT l.*, r.route_name FROM leaderboard l, routes r WHERE r.route_id = l.route_id"
                curs.execute(sql)
                row = curs.fetchone()
		resp["result"] = []
                while row is not None and row is not False:
			resp["result"].append(row)
			row = curs.fetchone()
		curs.close()
	except Exception as e:
		resp['error'] = str(e)
	return json.dumps(resp)

@app.route("/leaderboard/get/<int:board_id>")
def leaderboard_get(board_id):
	resp = {}
        try:
                db=get_db()
                curs = db.cursor(oursql.DictCursor)
                sql="SELECT l.*,u.full_name FROM leaderboard_users l, users u WHERE u.user_id = l.user_id AND l.board_id = ? AND l.id = (select id from leaderboard_users l2 where l2.user_id = l.user_id order by elapsed_time asc limit 1) ORDER BY elapsed_time ASC LIMIT 10"
                curs.execute(sql, [board_id])
                rows = curs.fetchall()
		for i in range(0, len(rows)):
			rows[i]["date_created"] = str(rows[i]["date_created"])
			rows[i]["elapsed_time"] = "%02.3f" % (rows[i]["elapsed_time"]/1000.0)
                resp["result"] = rows
                curs.close()
        except Exception as e:
                resp['error'] = str(e)
	return json.dumps(resp)


# User Profile
@app.route("/user/profile/<int:user_id>", methods=['GET'])
def user_profile(user_id):
	resp = {}
        try:
                db=get_db()
                curs = db.cursor(oursql.DictCursor)
                sql="SELECT * from users where user_id = ?" 
                curs.execute(sql, [user_id])
                row = curs.fetchone()
		del row['password']
		row['date_created'] = str(row['date_created'])
		row['last_seen'] = str(row['last_seen'])
                resp["result"] = row
                curs.close()
        except Exception as e:
                resp['error'] = str(e)
	return json.dumps(resp)

@app.route("/user/profile/<int:user_id>", methods=['POST'])
@auth_required()
def user_profile_set(user_id, **kwargs):
	resp = {}

        try:
		if user_id <> int(kwargs['uid']): raise Exception("denied")
                db=get_db()
                curs = db.cursor(oursql.DictCursor, string_limit=1000000)
		sql="UPDATE users SET age=?,gender=?,weight=?,picture=? WHERE user_id=?"

		data=(request.form['age'], request.form['gender'], request.form['weight'], request.form['picture'], user_id)
                curs.execute(sql, data)
                curs.close()
		resp['result'] = "Success"
        except Exception as e:
                resp['error'] = str(e)
		print e
	return json.dumps(resp)



# http://flask.pocoo.org/docs/tutorial/dbcon/
def get_db():
	if not hasattr(g, 'db'):
		g.db = oursql.connect(passwd=db_pass, db=db_name, host=db_host, user=db_user, raise_on_warnings=False)
	return g.db

@app.teardown_appcontext
def close_db(error):
	if hasattr(g, 'db'):
		g.db.close()

if __name__ == "__main__":
	app.run(host="74.207.227.160", port=8000, ssl_context=context, debug=True)

