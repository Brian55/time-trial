-- MySQL dump 10.11
--
-- Host: localhost    Database: android_timetrial
-- ------------------------------------------------------
-- Server version	5.5.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `challenge`
--

DROP TABLE IF EXISTS `challenge`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `challenge` (
  `challenge_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`challenge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `challenge`
--

LOCK TABLES `challenge` WRITE;
/*!40000 ALTER TABLE `challenge` DISABLE KEYS */;
/*!40000 ALTER TABLE `challenge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `challenge_data`
--

DROP TABLE IF EXISTS `challenge_data`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `challenge_data` (
  `challenge_id` int(11) NOT NULL,
  `date_created` int(11) NOT NULL,
  `data_type` varchar(255) NOT NULL,
  `data_payload` text NOT NULL,
  KEY `challenge_id` (`challenge_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `challenge_data`
--

LOCK TABLES `challenge_data` WRITE;
/*!40000 ALTER TABLE `challenge_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `challenge_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat`
--

DROP TABLE IF EXISTS `chat`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `chat` (
  `chat_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `chat`
--

LOCK TABLES `chat` WRITE;
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard`
--

DROP TABLE IF EXISTS `leaderboard`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `leaderboard` (
  `board_Id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) NOT NULL,
  PRIMARY KEY (`board_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `leaderboard`
--

LOCK TABLES `leaderboard` WRITE;
/*!40000 ALTER TABLE `leaderboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaderboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leaderboard_users`
--

DROP TABLE IF EXISTS `leaderboard_users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `leaderboard_users` (
  `board_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `elapsed_time` float NOT NULL,
  `sensor_data` text NOT NULL,
  KEY `board_id` (`board_id`,`user_id`),
  KEY `board_id_2` (`board_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `leaderboard_users`
--

LOCK TABLES `leaderboard_users` WRITE;
/*!40000 ALTER TABLE `leaderboard_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `leaderboard_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route_segments`
--

DROP TABLE IF EXISTS `route_segments`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `route_segments` (
  `segment_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_id` int(11) NOT NULL,
  `sequence_id` int(11) NOT NULL,
  `lon` float(255,8) NOT NULL,
  `lat` float(255,8) NOT NULL,
  PRIMARY KEY (`segment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `route_segments`
--

LOCK TABLES `route_segments` WRITE;
/*!40000 ALTER TABLE `route_segments` DISABLE KEYS */;
INSERT INTO `route_segments` VALUES (1,1,1,-82.41548157,28.05911827),(2,1,2,-82.41657257,28.05913734),(3,1,3,-82.41673279,28.05918121),(4,1,4,-82.41757965,28.05917549),(5,1,5,-82.41764069,28.06016922),(10,3,1,-82.41454315,28.05747604),(11,3,2,-82.41349030,28.05734444),(12,3,3,-82.41357422,28.06118774),(13,3,4,-82.41297150,28.06118774),(14,3,5,-82.41299438,28.05728722),(15,3,6,-82.41007996,28.05739975),(16,3,7,-82.40892029,28.05736351),(17,3,8,-82.40889740,28.06224823),(18,3,9,-82.40842438,28.06292915),(19,3,10,-82.40821075,28.06355476),(20,3,11,-82.40821075,28.06563759),(21,3,12,-82.41316986,28.06565666),(22,3,13,-82.41314697,28.06482315),(23,3,14,-82.41391754,28.06474686),(24,3,15,-82.41726685,28.06476593),(25,3,16,-82.41793060,28.06522179),(26,3,17,-82.41793060,28.06522179),(27,3,18,-82.41821289,28.06567574),(28,3,19,-82.41864014,28.06588364),(29,3,20,-82.41892242,28.06592178),(30,3,21,-82.41885376,28.06397057),(31,3,22,-82.41883087,28.06260872),(32,3,23,-82.41938782,28.06253242),(33,3,24,-82.41934967,28.06181335),(34,3,25,-82.41956329,28.06118774),(35,3,26,-82.41975403,28.06056213),(36,3,27,-82.41973114,28.05887794),(37,3,28,-82.41973114,28.05813980),(38,3,29,-82.41735077,28.05819511),(39,3,30,-82.41597748,28.05810165),(40,3,31,-82.41454315,28.05747604),(64,5,1,-82.41548157,28.05911827),(65,5,2,-82.41657257,28.05913734),(66,5,3,-82.41673279,28.05918121),(67,5,4,-82.41757965,28.05917549),(68,5,5,-82.41764069,28.06016922),(69,6,1,-82.41712952,28.06167030),(70,6,2,-82.41561127,28.06167030),(71,6,3,-82.41559601,28.06219101),(72,6,4,-82.41620636,28.06261826),(73,6,5,-82.41621399,28.06375313),(74,6,6,-82.41711426,28.06365967),(75,6,7,-82.41700745,28.06250381),(76,6,8,-82.41714478,28.06254196),(77,6,9,-82.41712952,28.06176567),(78,6,10,-82.41712952,28.06167030),(79,7,1,-82.71961212,27.97646904),(80,7,2,-82.71961212,27.97646904),(81,7,3,-82.71961212,27.97621727);
/*!40000 ALTER TABLE `route_segments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `routes` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_name` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `routes`
--

LOCK TABLES `routes` WRITE;
/*!40000 ALTER TABLE `routes` DISABLE KEYS */;
INSERT INTO `routes` VALUES (1,'USF1','0000-00-00 00:00:00',0),(3,'USF3','0000-00-00 00:00:00',0),(5,'USF5','0000-00-00 00:00:00',0),(6,'USF6','0000-00-00 00:00:00',0),(7,'Test','0000-00-00 00:00:00',0);
/*!40000 ALTER TABLE `routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `user_sessions`
--

LOCK TABLES `user_sessions` WRITE;
/*!40000 ALTER TABLE `user_sessions` DISABLE KEYS */;
INSERT INTO `user_sessions` VALUES (1,1,'2014-03-25 00:29:00','172.56.4.60','1.BhKXjA.nr1yVDhtbbEs2o1eoDD9Aqq1gVI'),(2,1,'2014-03-25 14:26:37','172.56.4.60','1.BhNb3Q.9oKW6Nkt8VXI5pdlZBDLVPSoXyY'),(3,1,'2014-03-25 14:29:16','172.56.4.60','1.BhNcfA.EHb5UMBXBfFW_CjBoEXztG4qI4k'),(4,1,'2014-03-25 14:33:13','172.56.4.60','1.BhNdaQ.GKpjEzON51qf8pErXHU4C6nJofU'),(5,1,'2014-03-25 14:34:26','172.56.4.60','1.BhNdsg.vmAiSrtR78PsxoxJE0F8C-ve8Ho'),(6,1,'2014-03-25 14:35:12','172.56.4.60','1.BhNd4A.0I_IgCHVNTeWD8Fs4jQHsj9DIAA'),(7,1,'2014-03-25 14:37:37','172.56.4.60','1.BhNecQ.UJZwGx-IlzKLR2YTIIyDFgtgkZc'),(8,1,'2014-03-25 14:38:32','172.56.4.60','1.BhNeqA.ItZOJTnzab8DzbuzH9WXzM5miu4'),(9,1,'2014-03-25 14:40:19','172.56.4.60','1.BhNfEw.DrfO4rAEI2attI1-DwjCSX3luYs'),(10,1,'2014-03-25 14:41:55','172.56.4.60','1.BhNfcw.IthPue3QIeCIslQHHPJAncXbSzs'),(11,1,'2014-03-25 14:44:55','172.56.4.60','1.BhNgJw.678x7_PYu2nerLr1x7a-uQBnnn4'),(12,1,'2014-03-25 14:47:23','172.56.4.60','1.BhNguw.6n10fqhpCAUSm9mAS1HdbdmI7Jc'),(13,2,'2014-03-25 15:22:19','172.56.4.60','2.BhNo6w.JsMjExJntw193bzvDLWaRY03NBE'),(14,2,'2014-03-25 16:01:36','172.56.4.60','2.BhNyIA.Vb6tpzEfcvruXBH6HPzFLTMU7yw'),(15,3,'2014-03-30 19:15:26','70.193.195.135','3.Bho3Dg.J4tIu4c6Nxx-1s4Er6-2MHkTowM'),(16,4,'2014-03-30 19:23:55','70.193.195.135','4.Bho5Cw.PyXx0dQsI_D2TzqwoER-LWLWC4A'),(17,4,'2014-03-30 21:41:13','70.193.195.135','4.BhpZOQ.F4msgUi7Z-mRbjqu4Feu4BVYOt0'),(18,4,'2014-03-30 21:41:20','70.193.195.135','4.BhpZQA.XnV1H19D4FyNAqMBU6AfRUTtd7c'),(19,4,'2014-03-30 21:41:57','70.193.195.135','4.BhpZZQ.rAHqWSWV2pGORpv3wVYxRh_aR6w'),(20,4,'2014-03-31 15:47:51','131.247.226.3','4.BhtX5w.V3wWyOsdJOpN92_cPbFOy3NRx0w'),(21,4,'2014-04-01 12:40:21','70.193.206.190','4.Bhx9dQ.SDOLxezWhc_LL-lGAKvIZKHUW_c'),(22,4,'2014-04-01 12:40:36','70.193.206.190','4.Bhx9hA.BNAl9i-fuv6WmSCCy7CPfLiwCXA'),(23,12,'2014-04-01 12:41:16','70.193.206.190','12.Bhx9rA.i1V-3X6kvP2lkImP0DfIWt416oE'),(24,1,'2014-04-01 12:49:37','172.56.5.217','1.Bhx_oQ.AJbmi7YTo9oC2EvTNArfg5WV5WI'),(25,12,'2014-04-01 12:50:46','131.247.226.2','12.Bhx_5g.Wv0lYiWZ5PBh5QJnF3nkdeWpcmk'),(26,12,'2014-04-01 12:51:03','131.247.226.2','12.Bhx_9w.OU3v1BRCvcqMN5kzAMM-ne5CsBg'),(27,29,'2014-04-01 12:54:02','172.56.4.35','29.BhyAqg.ARyA32V5y5EBQhYM1lvxWGdDztk'),(28,29,'2014-04-01 12:55:17','131.247.224.3','29.BhyA9Q.fSURF9-eR3igOYEuq53v0jgD8J4'),(29,29,'2014-04-01 12:56:13','131.247.224.3','29.BhyBLQ.kL7CL6lu6J6hkxaZUZUlMqRc3Jw'),(30,29,'2014-04-01 13:00:49','131.247.224.3','29.BhyCQQ.RQxRwdXsfWeHMRxCOPzp3S03d8U'),(31,31,'2014-04-01 13:05:25','172.56.5.217','31.BhyDVQ.Bsf6BYpTWjZ-y7BrjhvqoenI4dg'),(32,31,'2014-04-01 13:22:45','172.56.5.217','31.BhyHZQ.vKp5cw21NV2NyfB-unrHfIpWReA'),(33,4,'2014-04-01 13:51:52','131.247.226.2','4.BhyOOA.sGvw8VZosb9JvZlovNvW3hWzGoM'),(34,4,'2014-04-01 14:06:15','131.247.226.2','4.BhyRlw.nRaSslCRiby9rWIF9sDGRwbcoVA'),(35,4,'2014-04-01 14:06:45','131.247.226.2','4.BhyRtQ.BrH76Fr5lYJWfDZ0CjF8XtmDDaE'),(36,4,'2014-04-01 14:08:26','131.247.226.2','4.BhySGg.f5j-hgjASeBYZuO4AGKlGdE8HEo'),(37,4,'2014-04-01 14:19:10','131.247.226.2','4.BhyUng.mP62Y0UqdUYZW0dYfkcGtjhcnkc'),(38,4,'2014-04-01 14:33:20','131.247.226.2','4.BhyX8A.EqnoMDbFgSdaPd5bWxR43Wd5V_Y'),(39,4,'2014-04-01 14:35:07','131.247.226.2','4.BhyYWw.WRk_keyjUVc07ElZOTOaU9n2Y4w'),(40,4,'2014-04-01 14:36:40','131.247.226.2','4.BhyYuA.kaMTFdunZdpMLEe9w9nPbJVtwvU'),(41,4,'2014-04-01 14:38:03','131.247.226.2','4.BhyZCw.qfHCIr5STXO0Js8223ELVroYoy0'),(42,4,'2014-04-01 14:43:03','131.247.226.2','4.BhyaNw.WXhGn_4vcNCwwBXROyffhC20aYA'),(43,4,'2014-04-01 15:46:37','70.193.205.221','4.BhypHQ.4EjrHhNOPXgTC0fClvwDUqnj2LA'),(44,4,'2014-04-01 16:37:35','70.193.205.221','4.Bhy1Dw.DhYmWqnXP1WLVVVwhJ0ptlRys7k'),(45,4,'2014-04-02 14:44:23','172.56.29.85','4.Bh3sBw.OtH_vl2nI4B5eWJKbB2_pOITv7I'),(46,4,'2014-04-02 14:46:46','172.56.29.85','4.Bh3slg.khkAuhDPpBMSXLFo6E3u-QEEuAc'),(47,4,'2014-04-02 14:48:12','172.56.29.85','4.Bh3s7A.RPMgYE4hFTziopJYSaaOS7drFFs'),(48,4,'2014-04-02 14:51:32','172.56.29.85','4.Bh3ttA.SzvN_bby3Fko6JrBfQqd9fM170o'),(49,4,'2014-04-02 14:56:36','172.56.29.85','4.Bh3u5A.41ZnyKgvpD_DHmpErNEDgehFpHA'),(50,4,'2014-04-02 22:08:19','70.193.207.59','4.Bh5UEw.6Gz2MWMoAtuDPxqj2-suNrNe2QE'),(51,4,'2014-04-02 22:08:24','70.193.207.59','4.Bh5UGA.KR5zh0Is2vW-ar-XwXWqWfGQ_4w'),(52,4,'2014-04-02 22:08:25','70.193.207.59','4.Bh5UGQ.7XBE702zMWhtApJTx3_BLH1fZyU'),(53,4,'2014-04-02 22:08:28','70.193.207.59','4.Bh5UHA.EO_GxjPeiLnmP8akh1ksglZ2qy8'),(54,4,'2014-04-02 22:08:29','70.193.207.59','4.Bh5UHQ.28y5qOR84kHIk0pYxE6udeVMY-A'),(55,4,'2014-04-02 22:08:29','70.193.207.59','4.Bh5UHQ.28y5qOR84kHIk0pYxE6udeVMY-A'),(56,4,'2014-04-02 22:08:30','70.193.207.59','4.Bh5UHg.lpUNd-Rp1qlh6fWc-qVquQExxfs'),(57,4,'2014-04-02 22:08:30','70.193.207.59','4.Bh5UHg.lpUNd-Rp1qlh6fWc-qVquQExxfs'),(58,4,'2014-04-03 15:20:41','172.56.0.156','4.Bh9GCQ.LuD-PTc4IPHx7qI8aSXmLbRByp8'),(59,4,'2014-04-03 15:22:52','172.56.0.156','4.Bh9GjA.NqTzvFK-7b8tNVz2lodfeBXvCPE'),(60,4,'2014-04-03 15:25:51','172.56.0.156','4.Bh9HPw.szADoRtSASX-_4GkrDoZ_qpAmNg'),(61,4,'2014-04-03 15:35:03','172.56.0.156','4.Bh9JZw.sNVEveXptE7iUjzfDT52NAS0v8I'),(62,4,'2014-04-03 15:37:36','172.56.0.156','4.Bh9KAA.hlqTxuLKG1zV79pEm3WVZyF2j-g'),(63,4,'2014-04-03 15:38:40','172.56.0.156','4.Bh9KQA.6TyL7Ho98rYB-HUcTNYkINVXgR0'),(64,4,'2014-04-03 15:44:55','172.56.0.156','4.Bh9Ltw.VpGJYjx1BjoFDYsRPpGye3CjPBU'),(65,4,'2014-04-03 15:55:40','172.56.0.156','4.Bh9OPA.FvFeuvij_kg_QNv_hvLDq-zCDhk'),(66,4,'2014-04-03 16:00:22','172.56.0.156','4.Bh9PVg.IQuKDvNNl4MtJg2oRLeezbRwJDQ'),(67,4,'2014-04-03 16:03:09','172.56.0.156','4.Bh9P_Q.cw4icWfgJhxdeL8nNlJAesJDVOg'),(68,4,'2014-04-03 16:04:16','172.56.0.156','4.Bh9QQA.4VsriHXbUemm-KxXWBsuD8bE_Uo'),(69,4,'2014-04-03 16:10:21','172.56.0.156','4.Bh9RrQ.QPVM-RYu1-rXTWV_dboT0l49jkw'),(70,4,'2014-04-03 16:10:31','70.193.192.65','4.Bh9Rtw.jv-rf9GAclW0e28zGglvVlkXZx8'),(71,4,'2014-04-03 16:15:05','172.56.0.156','4.Bh9SyQ.rQDnKaodvPW2zPinsJLR0Hgm_qI'),(72,4,'2014-04-03 16:17:16','172.56.0.156','4.Bh9TTA.BFzXG_zuGnY8bb689-QcvChzsac'),(73,4,'2014-04-03 16:32:33','70.193.192.65','4.Bh9W4Q.hcF9HwMMQRmr9aSkzWegnKtgvA8'),(74,4,'2014-04-03 16:35:11','70.193.192.65','4.Bh9Xfw.rJfqYvkURV9n59G99Ub4Is2-Ftg'),(75,4,'2014-04-03 16:43:23','172.56.0.156','4.Bh9Zaw.ZO_gQlrWIGLaDJTM-KeXxfVad_4'),(76,4,'2014-04-03 16:44:06','70.193.192.65','4.Bh9Zlg.BKz_Vpmt4OQqy8EhUEIyVq9zmrM'),(77,4,'2014-04-03 16:46:30','70.193.192.65','4.Bh9aJg.Ra-z2K_eqRnfQQhahEnTFnZyAtk'),(78,4,'2014-04-03 16:49:47','172.56.0.156','4.Bh9a6w.fuz0-kmHiaD997i4gIieM2Naxv8'),(79,4,'2014-04-03 16:51:50','172.56.0.156','4.Bh9bZg.WTiLO4yFCzxSHGNxKDUQV7AiHeA'),(80,4,'2014-04-03 16:53:26','70.193.192.65','4.Bh9bxg.IPISw1hSotIfdDq7AsxVRlmZcIs'),(81,29,'2014-04-03 16:58:42','131.247.224.2','29.Bh9dAg.-OMuOcJcAnbxjO6tFnmrHXuNcE0'),(82,29,'2014-04-03 16:59:49','131.247.224.2','29.Bh9dRQ.q3Gxm8Cylt3sDP_8wpNahLJ-q8s'),(83,4,'2014-04-03 16:59:57','70.193.192.65','4.Bh9dTQ.kmcOVf2tZo5Ln1OIF2rfZ335JW4'),(84,4,'2014-04-03 17:02:20','70.193.192.65','4.Bh9d3A.VPk1KkE5GCT1HEfAPlHQ8LsAgCw'),(85,29,'2014-04-03 17:03:47','131.247.224.2',''),(86,4,'2014-04-03 17:04:27','131.247.224.2',''),(87,4,'2014-04-03 17:04:36','131.247.224.2','4.Bh9eZA.pTFGAqIa5OlA-wQjSpb5P5McWws'),(88,4,'2014-04-03 17:05:23','70.193.192.65','4.Bh9ekw.q28vJUGbzY9pP4Zpq-39LA57oaM'),(89,4,'2014-04-03 17:05:33','131.247.224.2','4.Bh9enQ.nD7Y6BwF34GoJ0kGymQ_sRpApYs'),(90,4,'2014-04-03 17:06:34','131.247.224.2','4.Bh9e2g.5RykAkM4AEwcKeOjoaCLpuHzm3w'),(91,4,'2014-04-03 17:07:49','70.193.192.65','4.Bh9fJQ.9qaIcRcAQbzY1Py6XKdHfUGG2CM'),(92,4,'2014-04-03 17:08:42','131.247.224.2','4.Bh9fWg.kNy0ltth_8bJCfl2NZyU2c3OrEE'),(93,4,'2014-04-03 17:10:13','131.247.224.2','4.Bh9ftQ.ZBgRKwkEPV3JBHkRtnJwe07R2vM'),(94,4,'2014-04-03 17:13:00','131.247.224.2','4.Bh9gXA.T3sB6fAFPSv-X1QWVRb7U8R6Pvg'),(95,4,'2014-04-03 17:13:47','131.247.224.2','4.Bh9giw.hvyP_LeuSkKGMLipHL1zIFhNkG8'),(96,4,'2014-04-03 17:14:18','131.247.224.2','4.Bh9gqg.J4J9XKXDGBadL9cmHV_goEn-Vxo'),(97,4,'2014-04-03 17:14:37','131.247.224.2','4.Bh9gvQ.wlx_z1w9JeZDPuMeYEPnJneD0ls'),(98,4,'2014-04-03 17:14:53','131.247.224.2','4.Bh9gzQ.sXmP-9knAAOokpBqyb4JioiU3as'),(99,4,'2014-04-03 17:15:22','70.193.192.65','4.Bh9g6g.9D5tSaxJZL0ct8upFPz0iglVrSw'),(100,4,'2014-04-03 20:25:39','70.193.192.65','4.Bh-Ngw.3piq4mA8o8-XoN6qEbdwFmTlGWU'),(101,4,'2014-04-03 20:28:42','70.193.192.65','4.Bh-OOg.4SHsplwbgoblNqGQ_DtT5KOQ6xs'),(102,4,'2014-04-03 20:29:28','70.193.192.65','4.Bh-OaA.Tn3WML9OfJSUkUb2RL3Y52z8Ln0'),(103,4,'2014-04-03 20:30:05','70.193.192.65','4.Bh-OjQ.UPGcQiPx3d-GbKK7ehQHWxJCsQ8'),(104,4,'2014-04-03 20:38:48','70.193.192.65','4.Bh-QmA.r3y9JyoUAtwDb7ST7-aYKWijxy8'),(105,4,'2014-04-03 20:42:21','70.193.192.65','4.Bh-RbQ.mqzM7c_8LpK_7mf0PaTJgWDY0M0'),(106,4,'2014-04-03 20:44:15','70.193.192.65','4.Bh-R3w.5zYEo9ONG44TokMHNW6GgEkQ29M'),(107,4,'2014-04-03 20:46:37','70.193.192.65','4.Bh-SbQ.HUarMx3_OUQ6o7O7PJIaExoRpCE'),(108,4,'2014-04-03 20:47:51','70.193.192.65','4.Bh-Stw.pXooT32QvZ3XfB1gvV0mnPzorew'),(109,4,'2014-04-03 20:50:07','70.193.192.65','4.Bh-TPw.hD06YCWYk1ABwwSAkFRXAbEtkW0'),(110,4,'2014-04-05 00:08:25','172.56.5.2','4.BiETOQ.efJ6a6ZYiXJTC-jej-5xypz7cwQ'),(111,4,'2014-04-05 00:10:01','172.56.5.2','4.BiETmQ.sxqcvg_XQtWPf60ge4fIvssRHFo'),(112,4,'2014-04-05 00:31:51','172.56.5.2','4.BiEYtw.E07xcAxDQgOJckYC_UblwVfAJZk'),(113,4,'2014-04-05 00:37:08','172.56.5.2','4.BiEZ9A.cQLbyQ5eq_cfoar2-BFHL2ceo7Y'),(114,4,'2014-04-05 00:38:57','172.56.5.2','4.BiEaYQ.g--UADNh1_MhJaJIt8ki27I58F4'),(115,4,'2014-04-05 00:39:55','172.56.5.2','4.BiEamw.6wJI1ZbEQTIAkcbnQ2Kj5TwuaJ0'),(116,4,'2014-04-05 00:54:12','172.56.5.2','4.BiEd9A.78TETQEWWoxfcYvDPES2vzxGieQ'),(117,4,'2014-04-05 01:10:12','172.56.5.2','4.BiEhtA.L14F71ew7SHpWurdVqqY9a-L-vQ'),(118,4,'2014-04-05 01:16:34','172.56.5.2','4.BiEjMg.eJvhK0a4yTVuQQW1H4A4KROyZkw'),(119,4,'2014-04-05 01:20:03','172.56.5.2','4.BiEkAw.e8TuFhv6kmYuwbafsO9PnPHN5fw'),(120,4,'2014-04-05 01:21:32','172.56.5.2','4.BiEkXA.lPvfFa_Yov7BcFrSHVzlKTz0K7g'),(121,4,'2014-04-05 01:22:45','172.56.5.2','4.BiEkpQ.0bVowNcWrK9vTB-8DTuFC2Nf-kE'),(122,4,'2014-04-05 01:23:43','172.56.5.2','4.BiEk3w.3j5sU2OssGXkVZwO9plw9zINq4s'),(123,4,'2014-04-05 01:24:14','172.56.5.2','4.BiEk_g.luXy6F26Yo0tdGnds5ndkg_i2Ck'),(124,4,'2014-04-05 01:50:39','172.56.5.2','4.BiErLw.iGbg_pwz0KO9quW_OH4ZEEJ9uU4'),(125,4,'2014-04-05 02:00:12','172.56.5.2','4.BiEtbA.s6u3DAqEfFFz5K9EIvgz3KnfqTA'),(126,4,'2014-04-05 02:07:37','172.56.5.2','4.BiEvKQ.SXbjHuvdcq7VtYn4n6iw9TYDuTA'),(127,4,'2014-04-05 02:10:27','172.56.5.2','4.BiEv0w.cdVG1kujvcbfV2GSbFqgMBWG-PM'),(128,4,'2014-04-05 02:12:57','172.56.5.2','4.BiEwaQ.iUbtqtU0MZjeFCkMXjZAljv3nwc'),(129,4,'2014-04-05 02:15:32','172.56.5.2',''),(130,4,'2014-04-05 02:17:40','172.56.5.2','4.BiExhA.96aaUoNofwLOPVHI4UN6AU1Tyzg'),(131,4,'2014-04-05 16:06:14','172.56.5.2','4.BiHztg.343zRGPHUyjxsrdOirNkWmXWbd0'),(132,4,'2014-04-05 16:10:22','172.56.5.2','4.BiH0rg.XxQJhtB4NkGKIzcdrT6NZfYqYCw'),(133,4,'2014-04-05 16:14:10','172.56.5.2','4.BiH1kg.CtLipxjAyMExYHz3SxPKpl8wmhY'),(134,4,'2014-04-05 16:29:18','172.56.5.2','4.BiH5Hg.z3pn_zHrgv46W21N1YQvq60_GSc'),(135,4,'2014-04-05 16:32:41','172.56.5.2','4.BiH56Q.glgeUrx1kWX5UgRLZtRyxpX1o70'),(136,4,'2014-04-05 16:39:14','172.56.5.2','4.BiH7cg.rVQ-xFDA6aq5-kIUCV4UQq9fS80'),(137,4,'2014-04-05 16:41:44','172.56.5.2','4.BiH8CA.QYFghyvm33dziMiTOYeDZraJIMw'),(138,4,'2014-04-05 17:25:55','172.56.5.2','4.BiIGYw.r92kXrR4IBC4AVH1-FvES_82cC4'),(139,4,'2014-04-05 17:27:06','172.56.5.2','4.BiIGqg.pxR1z_X5CV8jqaEm5ZwaC5vRoC4'),(140,4,'2014-04-05 17:28:50','172.56.5.2','4.BiIHEg.sdavHWKfOPx_R7wk3xWPI8o9V3k'),(141,4,'2014-04-05 18:26:31','172.56.5.2','4.BiIUlw.C4uMBh4dy9ToELu2JdPJD5vnQ7o'),(142,4,'2014-04-05 18:38:08','172.56.5.2','4.BiIXUA.tfxZqLwjgj8HVPMkFzdSxzJQ0oE'),(143,4,'2014-04-05 18:39:09','172.56.5.2','4.BiIXjQ.grCs1ixCXElpWJAmhr_NOzBm9gU'),(144,4,'2014-04-05 18:40:21','172.56.5.2','4.BiIX1Q.62t8RgI4Cs_fBgC5cwBHPAyaSg8'),(145,4,'2014-04-05 18:42:00','172.56.5.2','4.BiIYOA.yv0aQ7k6BuCE0XaUfxJGTUIXuSI'),(146,4,'2014-04-05 18:45:27','172.56.5.2','4.BiIZBw.c9GgnYWyUpC-QZ--DKehSYZghDQ'),(147,4,'2014-04-05 18:53:17','172.56.5.2','4.BiIa3Q.78AXokVojjqDO1XgYRPCzqHgkeU');
/*!40000 ALTER TABLE `user_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `last_lat` float(255,8) DEFAULT NULL,
  `last_long` float(255,8) DEFAULT NULL,
  `last_seen` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'test','test','$P$EOHU2uNeVikO6J9AHsSAE6KGoc4Wrk.','2014-03-25 00:29:00',28.05881119,-82.41519165,'2014-04-01 12:49:38'),(2,'hello','icmp','$P$EwiY9aI/aQbQYWbr15CR/.USbGh/Wi/','2014-03-25 15:22:19',28.05932045,-82.41333008,'2014-03-25 16:01:38'),(3,'admin','admin@admin.com','$P$EA3pKwJJKmXP2q0sDcr28sT4Kcc9Dm1','2014-03-30 19:15:26',NULL,NULL,'2014-03-30 19:15:26'),(4,'','','$P$Etu4v9bqSRsEQjnc45DuR.d/B.tmHO1','2014-03-30 19:23:55',27.97635269,-82.71943665,'2014-04-05 18:53:19'),(12,'brian','briank@mail.usf.edu','$P$EF6STQfz5azxKoY5Ie7Lf6NqguIh5v.','2014-04-01 12:41:15',28.05882454,-82.41514587,'2014-04-01 12:51:38'),(29,'Robert Niznik','niznikr@mail.usf.edu','$P$EQ2lWGT6cy7QtbszZ9biddzD8FL7MU.','2014-04-01 12:54:02',28.05882454,-82.41519928,'2014-04-03 17:03:48'),(31,'abc','bc@def.com','$P$EuSATwpNqi2Nvg.tlXwCSbginllGqC0','2014-04-01 13:05:25',28.05881500,-82.41518402,'2014-04-01 13:22:47');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-04-06 13:51:44
